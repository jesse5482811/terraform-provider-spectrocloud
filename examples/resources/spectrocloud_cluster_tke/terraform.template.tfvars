# Spectro Cloud credentials
sc_host         = "{enter Spectro Cloud API endpoint}"
sc_api_key      = "{enter Spectro Cloud API endpoint}"
sc_project_name = "Default"
sc_trace        = false

tke_ssh_key_name = "{enter Spectro Cloud ssh key for tke}"
tke_region       = "{enter region name for tke cluster}"
tke_vpc_id       = "{enter tke vpc id}"
master_tke_subnets_map = {
  "{enter subnet key}" : "{enter subnet id}"
}
worker_tke_subnets_map = {
  "{enter subnet key}" : "{enter subnet id}"
}